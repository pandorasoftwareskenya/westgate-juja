const withPWA = require("next-pwa");
const withAntdLess = require("next-plugin-antd-less");
const withPlugins = require("next-compose-plugins");

// optional next.js configuration
const nextConfig = {
  env: {
    MPESA_CONSUMER_KEY: "NrX8aIAPx1IXHjigTl87MLJPfmxHIWEG",
    MPESA_CONSUMER_SECRET: "o6eA7440FApu2MHb",
    API_KEY: "AIzaSyBV7SKK_QgP0h8EyBYUoJLtn74pyJVZlcQ",
    AUTH_DOMAIN: "westgate-shop.firebaseapp.com",
    PROJECT_ID: "westgate-shop",
    STORAGE_BUCKET: "westgate-shop.appspot.com",
    MESSAGING_SENDER_ID: "988886493639",
    APP_ID: "1:988886493639:web:ceaf61be4e221a7f5151e2",
  },
  reactStrictMode: true,
  images: {
    domains: ["firebasestorage.googleapis.com"],
  },
};

module.exports = withPlugins(
  [
    // add a plugin with specific configuration
    [
      withPWA,
      {
        dest: "public",
        register: true,
        skipWaiting: true,
        disable: true,
        // disable: process.env.NODE_ENV === "development",
      },
    ],

    // another plugin with a configuration
    [
      withAntdLess,
      {
        // optional: you can modify antd less variables directly here
        modifyVars: { "@primary-color": "#3F9B42" },
        // Or better still you can specify a path to a file
        // lessVarsFilePath: "./styles/variables.less",
        // optional
        lessVarsFilePathAppendToEndOfContent: false,
        // optional https://github.com/webpack-contrib/css-loader#object
        cssLoaderOptions: {},
      },
    ],
  ],
  nextConfig
);
