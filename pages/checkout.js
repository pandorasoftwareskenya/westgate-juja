import { useState } from "react";
import { Page, Container, Button } from "../components";
import {
  Steps,
  Card,
  Radio,
  Space,
  Tag,
  Button as ButtonAntd,
  Typography,
  message,
  Tooltip,
} from "antd";
import { useAuth } from "../context";
import { useRouter } from "next/router";

import { db } from "../firebase/clientApp";

import { doc } from "firebase/firestore";
import { useDocumentOnce } from "react-firebase-hooks/firestore";
import { WarningOutlined } from "@ant-design/icons";
import DB_Services from "../firebase/services";

const { Step } = Steps;
const { Text } = Typography;

export default function Checkout() {
  const { authUser, loading } = useAuth();
  const router = useRouter();

  const [loadingOrdering, setLoadingOrdering] = useState(false);

  const [user, userLoading, userError] = useDocumentOnce(
    doc(db, "users", authUser ? authUser.uid : "unfindalble"),
    {}
  );

  const [paymentMethod, setPaymentMethod] = useState("mpesa-now");
  const [current, setCurrent] = useState(0);

  const onChangePaymentMethod = (e) => {
    setPaymentMethod(e.target.value);
    setCurrent(2);
  };

  const completeOrder = () => {
    if (paymentMethod == "mpesa-now") {
      //initiate stk , change record to paid , add transaction code
      message.info(
        "M-pesa not yet functional. Please select the other payment option"
      );
    } else {
      //deliver first
      if (user.data().location == ("" || null)) {
        message.warn(
          `Missing location. Enter your building name and house number(optional) as our location details.Heading over to account page`
        );
        router.push("/account");
      } else if (user.data().phoneNumber == ("" || null)) {
        message.warn(
          `Add your phone number to complete your order. Heading over to account page`
        );
        router.push("/account");
      } else if (
        user.data().phoneNumber != ("" || null) &&
        user.data().location != ("" || null)
      ) {
        setLoadingOrdering(true);
        DB_Services.setDeliverFirst({
          user: authUser.uid,
          ids: getIds(user.data()),
        })
          .then((val) => console.log(val))
          .then(() => {
            setLoadingOrdering(false);
            message.success("Order complete. Await packing and delivery");
            router.push("/");
          });
      }
    }
  };

  const getIds = (data) => {
    let ids = [];

    data.cart
      .filter((cartItem) => cartItem.paid == false)
      .forEach((cartItem) => {
        ids[ids.length] = cartItem.product.id;
      });

    console.log(ids);

    return ids;
  };

  const Delivery = ({ data }) => {
    return (
      <div style={{ width: "100%" }}>
        <br />
        <Card
          size="small"
          title="Address details"
          style={{ width: "100%", height: 230 }}
        >
          <div style={{ color: "#707070", padding: 8 }}>
            <h3>{data.displayName}</h3>
            <p>{data.phoneNumber}</p>
            <p style={{ color: "#3F9B42" }}>{data.location}</p>
          </div>
          <div
            style={{
              padding: 8,
              display: "flex",
              width: "100%",
              position: "relative",
              justifyContent: "space-between",
            }}
          >
            {data.location == ("" || null) ||
              (data.phoneNumber == ("" || null) && (
                <>
                  <Text type="warning" style={{ lineHeight: 2.2 }}>
                    <WarningOutlined style={{ marginRight: 8 }} />
                    Missing location and/ phone number
                  </Text>
                  <ButtonAntd
                    type="link"
                    onClick={() => router.push("/account")}
                  >
                    Go to account
                  </ButtonAntd>
                </>
              ))}
          </div>
        </Card>
      </div>
    );
  };

  const Payment = () => {
    return (
      <div style={{ padding: "16px 0px" }}>
        <Radio.Group onChange={onChangePaymentMethod} value={paymentMethod}>
          <Space direction="vertical">
            <Radio value={"mpesa-now"}>Pay now - M-pesa</Radio>
            <Radio value={"on-delivery"}>Pay on delivery</Radio>
          </Space>
        </Radio.Group>
      </div>
    );
  };

  const Summary = ({ data }) => {
    const getTotal = () => {
      let tot = 0;
      data.cart.forEach((cartItem) => {
        if (cartItem.paid == false && cartItem.deliver_first == false) {
          tot += cartItem.quantity * cartItem.product.price;
        }
      });
      return tot;
    };

    return (
      <div>
        <br />
        <Card
          size="small"
          title="Total"
          extra={<span style={{ color: "#3F9B42" }}>Ksh. {getTotal()}</span>}
          style={{ width: "100%", marginBottom: "2rem" }}
        >
          <div style={{ color: "#707070", padding: 8 }}>
            {data.cart
              .filter(
                (cartItem) =>
                  cartItem.paid == false && cartItem.deliver_first == false
              )
              .map((cartItem, index) => {
                return (
                  <div
                    style={{
                      borderBottom: "#f1f1f1 1px dashed",
                      display: "flex",
                      justifyContent: "space-between",
                      padding: "8px 0px",
                    }}
                    key={index}
                  >
                    <Tooltip
                      title={cartItem.product.name}
                      color="#3F9B42"
                      trigger="click"
                    >
                      <Text ellipsis style={{ width: 120 }}>
                        {cartItem.product.name}
                      </Text>
                    </Tooltip>

                    <Tag color="green" style={{ height: 24, width: 24 }}>
                      {cartItem.quantity}
                    </Tag>
                    <span>@</span>
                    <span>{cartItem.product.price}</span>
                    <span>{cartItem.quantity * cartItem.product.price}</span>
                  </div>
                );
              })}
          </div>
          <br />
          <div style={{ padding: 8, marginBottom: 6 }}>Delivery fee : 0</div>

          <span style={{ padding: 8 }}>
            Payment method :{" "}
            <Tag color="green">
              {paymentMethod == "mpesa-now" ? "MPESA NOW" : "PAY ON DELIVERY"}
            </Tag>
          </span>
        </Card>
      </div>
    );
  };

  if (userLoading) return <p>Loading....</p>;

  if (typeof user.data() !== "undefined") {
    return (
      <Page title="Checkout">
        <Container
          style={{
            position: "absolute",
            top: 56,
            width: "calc(100% - 16px)",
            paddingBottom: "8rem",
          }}
        >
          <br />
          <Steps direction="vertical" size="small" current={current}>
            <Step
              title="Delivery"
              description={<Delivery data={user.data()} />}
            />
            <Step title="Payment" description={<Payment />} />
            <Step
              title="Summary"
              description={<Summary data={user.data()} />}
            />
          </Steps>
          <ButtonAntd
            loading={loadingOrdering}
            onClick={() => completeOrder(user.data())}
            style={{
              marginBottom: "2rem",
              marginLeft: "5%",
              width: "90%",
              fontFamily: "Metropolis-Regular",
              color: "#3F9B42",
              display: "block",
              fontSize: "1.1rem",
              padding: "0.9rem",
              backgroundColor: "#E5E055",
              fontWeight: "900",
              height: 50,
              textTransform: "uppercase",
              color: "#fff",
              border: "none",
              borderRadius: "8px",
            }}
          >
            Complete order
          </ButtonAntd>
        </Container>
      </Page>
    );
  } else {
    return <p>Error</p>;
  }
}
