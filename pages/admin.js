import { Page, Container } from "../components";
import {
  Input,
  Form,
  Typography,
  InputNumber,
  Select,
  Upload,
  message,
  Row,
  Image,
  Col,
  Button,
  Tabs,
  Popconfirm,
  DatePicker,
  Tag,
  Divider,
  Menu,
  Dropdown,
  Modal,
} from "antd";
import { useState, useEffect } from "react";
import ImgCrop from "antd-img-crop";

import {
  DownOutlined,
  FilterOutlined,
  LoadingOutlined,
  MoreOutlined,
  UserOutlined,
  SearchOutlined,
  PlusOutlined,
} from "@ant-design/icons";
import DB_Services from "../firebase/services";
import Avatar from "antd/lib/avatar/avatar";
import Checkbox from "antd/lib/checkbox/Checkbox";
import Compressor from "compressorjs";

import { db } from "../firebase/clientApp";
import { collection, query, where } from "firebase/firestore";
import { useCollection } from "react-firebase-hooks/firestore";

const { Option } = Select;
const { RangePicker } = DatePicker;
const { TabPane } = Tabs;
const { Title, Text, Paragraph } = Typography;

export default function Admin() {
  return <Dashboard />;
}

const Dashboard = () => {
  // const signOut = () => {
  //   console.log("sign out");
  // };

  // const SignOutBtn = () => {
  //   return (
  //     <Popconfirm
  //       onConfirm={signOut}
  //       cancelText="No"
  //       okText="Yes"
  //       title="Are you sure?"
  //     >
  //       <Button type="link" style={{ color: "#E5E055" }}>
  //         Sign out
  //       </Button>
  //     </Popconfirm>
  //   );
  // };

  const ProductsComponent = (product) => {
    const [addModal, setAddModal] = useState(false);
    const [productName, setProductName] = useState("");
    const [productPrice, setProductPrice] = useState();
    const [productQuantity, setProductQuantity] = useState();
    const [category, setCategory] = useState("snacks");
    const [imageLoading, setImageLoading] = useState(false);
    const [image, setImage] = useState();
    const [imageUrl, setImageURL] = useState();
    const [uploadLoading, setLoadingUpload] = useState(false);

    useEffect(() => {
      if (image) {
        new Compressor(image, {
          quality: 0.6, // 0.6 can also be used, but its not recommended to go below.
          success: (compressedResult) => {
            // compressedResult has the compressed file.
            // Use the compressed file to upload the images to your server.
            const _imgURL = URL.createObjectURL(compressedResult);
            setImageURL(_imgURL);
            console.log(_imgURL);
          },
        });
      }
      return;
    }, [image]);

    const [products, productsLoading, productsError] = useCollection(
      collection(db, "products"),
      {}
    );

    const handleOkAdd = () => {
      if (!productName) {
        message.warning("Product name required to save image!");
      } else {
        setLoadingUpload(true);

        DB_Services.uploadProductImage(productName, imageUrl)
          .then((productImageURL) => {
            let _product = {
              name: productName,
              price: productPrice,
              quantity: productQuantity,
              category,
              image: productImageURL,
            };
            console.log(_product);
            DB_Services.createProduct(_product)
              .then(() => {
                message.success(
                  `Successfully added '${productName}' to the database`
                );
              })
              .then(() => setLoadingUpload(false))
              .then(() => {
                setAddModal(false);
                setCategory("snacks");
                setProductQuantity(null);
                setProductName(null);
                setProductPrice(null);
                setImage();
                setImageURL();
              })
              .catch((err) => message.error("Oops! Server error"));
          })
          .catch((err) => message.error("Failed to upload product image"));
      }
    };

    function getBase64(img, callback) {
      const reader = new FileReader();
      reader.addEventListener("load", () => callback(reader.result));
      reader.readAsDataURL(img);
    }

    function beforeUpload(file) {
      console.log("beforeUpload: fired!");
      const isJpgOrPng =
        file.type === "image/jpeg" || file.type === "image/png";
      if (!isJpgOrPng) {
        message.error("You can only upload JPG/PNG file!");
      }
      const isLt2M = file.size / 1024 / 1024 < 2;
      if (!isLt2M) {
        message.error("Image must smaller than 2MB!");
      }
      return isJpgOrPng && isLt2M;
    }

    const handleChangeImage = (info) => {
      console.log("handleChangeImage: started");
      if (info.file.status === "uploading") {
        setImageLoading(true);
        console.log("handleChange: uploading!");
        return;
      }
      if (info.file.status === "done") {
        // Get this url from response in real world.
        console.log("handleChange: doneChanging!");
        getBase64(info.file.originFileObj, (imageUrl) => {
          console.log("handleChange: url_obtained!");
          setImageURL(imageUrl);
          setImageLoading(false);
        });
      }
    };

    const onImageChange = (e) => {
      setImage(e.target.files[0]);
    };

    const [keyword, setKeyword] = useState("");

    const AdminProduct = (data) => {
      const [offersModal, setOffersModal] = useState(false);
      const [editModal, setEditModal] = useState(false);
      const [offer_price, setOfferPrice] = useState();
      const [startDate, setStartDate] = useState();
      const [endDate, setEndDate] = useState();
      const [loading, setLoading] = useState(false);
      const [loadingEdit, setLoadingEdit] = useState(false);
      const [editedPrice, setEditedPrice] = useState(data.data.price);
      const [editedName, setEditedName] = useState(data.data.name);
      const [editedCategory, setEditedCategory] = useState(data.data.category);
      const [editedQuantity, setEditedQuantity] = useState(data.data.quantity);

      const addToOffers = () => {
        setOffersModal(true);
      };

      const confirmDelete = () => {
        DB_Services.deleteProduct({ id: data.id })
          .then(() => message.success("Product deleted"))
          .catch((err) => console.log(err));
      };

      const handleOkOffers = () => {
        if (offer_price == null || startDate == null || endDate == null) {
          message.warning("Please fill in the missing field");
        } else {
          setLoading(true);
          let update = {
            offer_price,
            startDate,
            endDate,
            id: data.id,
          };
          DB_Services.addOffer(update)
            .then(() =>
              message.success(
                `Successfully added '${data.data.name}' to offers!`
              )
            )
            .then(() => setLoading(false))
            .then(() => setOffersModal(false))
            .catch(() => message.error("Oops! Failed. Try again later"));
        }
      };

      const handleOkEdit = () => {
        setLoadingEdit(true);
        let _product = {
          id: data.id,
          name: editedName,
          price: editedPrice,
          category: editedCategory,
          quantity: editedQuantity,
        };
        DB_Services.updateProduct(_product)
          .then(() => message.success("Updated!"))
          .then(() => setLoadingEdit(false))
          .then(() => setEditModal(false));
      };

      const handleCancel = () => {
        setOffersModal(false);
      };

      const menu = (
        <Menu>
          <Menu.Item key="0">
            <a onClick={addToOffers}>Add to offers</a>
          </Menu.Item>
          <Menu.Divider />
          <Menu.Item key="3">
            <a onClick={() => setEditModal(true)}>Edit product</a>
          </Menu.Item>
          <Menu.Divider />
          <Menu.Item key="4">
            <Popconfirm
              title="Are you sure to delete this product?"
              onConfirm={confirmDelete}
              okText="Yes"
              cancelText="No"
            >
              <a>Delete product</a>
            </Popconfirm>
          </Menu.Item>
        </Menu>
      );

      return (
        <div
          style={{
            width: "100%",
            marginBottom: 0,
            zIndex: 99,
          }}
        >
          <Row
            style={{
              position: "relative",
              background: "#f1f1f1",
              marginBottom: 8,
              padding: 8,
              height: 116,
            }}
          >
            {DB_Services.verifyOnOffer(
              data.data.start_date,
              data.data.end_date
            ) && (
              <Tag
                color="green"
                style={{ position: "absolute", top: 0, left: 0, zIndex: 2 }}
              >
                OFFER
              </Tag>
            )}

            <Col span={8}>
              <Image
                alt="product_image"
                src={data.data.image || ""}
                width={100}
                height={100}
                style={{ objectFit: "cover" }}
              />
            </Col>
            <Col span={13}>
              <div style={{ maxHeight: 150 }}>
                <p style={{ display: "block", margin: "2px 0px" }}>
                  <strong>{data.data.name}</strong>
                </p>

                {DB_Services.verifyOnOffer(
                  data.data.start_date,
                  data.data.end_date
                ) && (
                  <span style={{ display: "flex" }}>
                    <p
                      style={{
                        display: "block",
                        margin: "8px 0px",
                        color: "#707070",
                      }}
                    >
                      Ksh. {data.data.offer_price}
                    </p>
                    <h3
                      style={{
                        textDecoration: "line-through",
                        lineHeight: 3,
                        marginLeft: 24,
                        fontSize: "0.7rem",
                      }}
                    >
                      Ksh {data.data.price}
                    </h3>
                  </span>
                )}

                {!DB_Services.verifyOnOffer(
                  data.data.start_date,
                  data.data.end_date
                ) && (
                  <p
                    style={{
                      display: "block",
                      margin: "8px 0px",
                      color: "#707070",
                    }}
                  >
                    Ksh. {data.data.price}
                  </p>
                )}

                <span>
                  <Text color="#E5E055" style={{ marginRight: 12 }}>
                    {data.data.quantity}
                  </Text>
                  <Tag
                    color={
                      data.data.category == "snacks"
                        ? "orange"
                        : data.data.category == "drinks"
                        ? "purple"
                        : data.data.category == "ingredients"
                        ? "green"
                        : data.data.category == "cleaning"
                        ? "blue"
                        : data.data.category == "toiletries"
                        ? "skyblue"
                        : data.data.category == "other"
                        ? "brown"
                        : "pink"
                    }
                  >
                    {data.data.category.toString().toUpperCase()}
                  </Tag>
                </span>
              </div>
            </Col>
            <Col span={1}>
              <Dropdown overlay={menu} trigger={["click"]}>
                <Button type="link" color="#707070">
                  <MoreOutlined />
                </Button>
              </Dropdown>
            </Col>
          </Row>
          <Modal
            title="Edit product"
            onOk={handleOkEdit}
            visible={editModal}
            onCancel={() => setEditModal(false)}
            okText={loadingEdit ? "Updating" : "Update"}
            confirmLoading={loadingEdit}
          >
            <Form>
              <Form.Item label="Product name">
                <Input
                  value={editedName}
                  onChange={(e) => setEditedName(e.target.value)}
                  size="large"
                />
              </Form.Item>
              <Form.Item label="Price">
                <InputNumber
                  addonBefore="KSH"
                  value={editedPrice}
                  style={{ width: "100%" }}
                  size="large"
                  onChange={(val) => setEditedPrice(val)}
                />
              </Form.Item>
              <Form.Item label="Category">
                <Select
                  style={{ width: "100%" }}
                  size="large"
                  defaultValue={editedCategory}
                  onChange={(category) => setEditedCategory(category)}
                >
                  {[
                    "snacks",
                    "drinks",
                    "ingredients",
                    "cleaning",
                    "toiletries",
                    "other",
                  ].map((category, index) => (
                    <Option key={category}>{category}</Option>
                  ))}
                </Select>
              </Form.Item>
              <Form.Item label="Quantity">
                <InputNumber
                  value={editedQuantity}
                  style={{ width: "100%" }}
                  size="large"
                  onChange={(val) => setEditedQuantity(val)}
                />
              </Form.Item>
            </Form>
          </Modal>
          <Modal
            visible={offersModal}
            title="Add To Offers"
            onOk={handleOkOffers}
            onCancel={handleCancel}
            confirmLoading={loading}
            okText={loading ? "Adding" : "Save"}
          >
            <Form>
              <Form.Item label="Offer price" name="offer_price">
                <InputNumber
                  addonBefore="KSH"
                  placeholder="Offer price"
                  value={offer_price}
                  onChange={(val) => setOfferPrice(val)}
                />
              </Form.Item>
              <Form.Item label="Offer validity" name="offer_validity">
                <RangePicker
                  showTime
                  style={{ width: "100%" }}
                  onCalendarChange={(val, dateString) => {
                    setStartDate(dateString[0]);
                    setEndDate(dateString[1]);
                  }}
                />
              </Form.Item>
            </Form>
          </Modal>
        </div>
      );
    };

    const uploadButton = (
      <div style={{ width: "100%" }}>
        {imageLoading ? <LoadingOutlined /> : <PlusOutlined />}
        <div style={{ marginTop: 8 }}>Upload</div>
      </div>
    );

    return (
      <>
        <Row style={{ width: "100%" }}>
          <Col span={22}>
            <Input
              value={keyword}
              placeholder="Search product..."
              suffix={<SearchOutlined />}
              onChange={(e) => setKeyword(e.target.value)}
            />
          </Col>
          <Col span={2}>
            <Button type="link" style={{ color: "#707070" }}>
              <FilterOutlined />
            </Button>
          </Col>
        </Row>
        <br />
        {!products && productsLoading && <p>Loading...</p>}
        {products &&
          products.docs.length > 0 &&
          products.docs
            .filter((prod) =>
              prod.data().name.toLowerCase().includes(keyword.toLowerCase())
            )
            .map((product) => {
              return (
                <AdminProduct
                  key={product.id}
                  data={product.data()}
                  id={product.id}
                />
              );
            })}
        {products && products.docs.length < 1 && <p>Add products</p>}
        <Button
          onClick={() => setAddModal(true)}
          style={{
            position: "fixed",
            bottom: "3rem",
            right: "0.5rem",
            width: 48,
            height: 48,
            borderRadius: "50%",
            background: "#3F9B42",
          }}
        >
          <PlusOutlined style={{ color: "white" }} />
        </Button>
        <Modal
          title="Add product"
          visible={addModal}
          okText={uploadLoading ? "Uploading" : "Save"}
          onOk={handleOkAdd}
          onCancel={() => setAddModal(false)}
          confirmLoading={uploadLoading}
        >
          <Form>
            <Form.Item label="Product name">
              <Input
                value={productName}
                size="large"
                onChange={(e) => setProductName(e.target.value)}
                placeholder="Name"
              />
            </Form.Item>
            <Form.Item label="Price">
              <InputNumber
                size="large"
                style={{ width: "100%" }}
                onChange={(val) => setProductPrice(val)}
                addonBefore="KSH"
                placeholder="Price"
                value={productPrice}
              />
            </Form.Item>
            <Form.Item label="Quantity">
              <InputNumber
                size="large"
                style={{ width: "100%" }}
                onChange={(val) => setProductQuantity(val)}
                placeholder="Quantity"
                value={productQuantity}
              />
            </Form.Item>
            <Form.Item label="Category">
              <Select
                style={{ width: "100%" }}
                size="large"
                defaultValue="snacks"
                onChange={(category) => setCategory(category)}
              >
                {[
                  "snacks",
                  "drinks",
                  "ingredients",
                  "cleaning",
                  "toiletries",
                  "other",
                ].map((category, index) => (
                  <Option key={category}>{category}</Option>
                ))}
              </Select>
            </Form.Item>
            <Form.Item label="Product Image">
              {image && imageUrl ? (
                <ImgCrop rotate>
                  <img
                    src={imageUrl}
                    style={{ width: "100%", objectFit: "cover" }}
                  />
                </ImgCrop>
              ) : (
                <input type="file" accept="image/*" onChange={onImageChange} />
              )}

              {/* <ImgCrop rotate style={{ width: "100vw" }}>
                <Upload
                  name="avatar"
                  listType="picture-card"
                  className="avatar-uploader"
                  showUploadList={false}
                  beforeUpload={beforeUpload}
                  onChange={handleChangeImage}
                >
                  {imageUrl ? (
                    <img
                      src={imageUrl}
                      alt="avatar"
                      style={{
                        width: 150,
                        height: 150,
                        objectFit: "cover",
                        margin: "36px 0px 0px 36px",
                      }}
                    />
                  ) : (
                    uploadButton
                  )}
                </Upload>
              </ImgCrop> */}
            </Form.Item>
          </Form>
        </Modal>
      </>
    );
  };

  const OrdersComponent = () => {
    const [keyword, setKeyword] = useState("");
    const [filter, setFilter] = useState("paid");

    const [users, usersLoading, usersError] = useCollection(
      collection(db, "users"),
      {}
    );

    const onChangePacked = (user) => {
      DB_Services.markPacked({ user }).then((val) => console.log(val));
    };

    const onChangeDelivered = (user) => {
      DB_Services.markDelivered({ user }).then((val) => console.log(val));
    };

    // const modelData = async () => {
    //   let _orders_ = [];

    //   let getOtherProducts = (user_id) => {
    //     return new Promise((resolve, reject) => {
    //       let products = [];
    //       for (let i = 0; i < orders.docs.length; i++) {
    //         if (orders.docs[i].data().user == user_id) {
    //           let x = {
    //             quantity: orders.docs[i].data().quantity,
    //             packed: orders.docs[i].data().packed,
    //             delivered: orders.docs[i].data().delivered,
    //             paid: orders.docs[i].data().paid,
    //             product: orders.docs[i].data().product,
    //           };
    //           products[i] = x;
    //         }
    //       }
    //       resolve(products);
    //     });
    //   };

    //   let getCustomerMeta = (user_id) => {
    //     return users.docs.filter((user) => user.id == user_id)[0].data();
    //   };

    //   //  for await (let order of orders.docs) {
    //   for (let p = 0; p < orders.docs.length; p++) {
    //     let _order = {};
    //     _order.products = await getOtherProducts(orders.docs[p].data().user);
    //     _order.user = getCustomerMeta(orders.docs[p].data().user);

    //     if (_orders_.length < 1) {
    //       _orders_[p] = _order;
    //       console.log(_orders_);
    //     }

    //     if (_orders_.length > 0) {
    //       console.log("new user");
    //       for (let order of _orders_) {
    //         if (order.user.displayName != _order.user.displayName) {
    //           _orders_[p] = _order;
    //           console.log(_orders_);
    //           return;
    //         }
    //         return;
    //       }
    //     }
    //   }

    //   console.log(_orders_);

    //   return _orders_;
    // };

    const getTotal = (arr) => {
      let tot = 0;
      if (arr == 0) {
        return tot;
      } else {
        arr.forEach((cartItem) => {
          tot += cartItem.quantity * cartItem.product.price;
        });
        return tot;
      }
    };

    const menuFilterOrders = (
      <Menu>
        <Menu.Item key="0">
          <a onClick={() => setFilter("paid")}>Paid</a>
        </Menu.Item>
        <Menu.Divider />
        <Menu.Item key="3">
          <a onClick={() => setFilter("packed")}>Packed</a>
        </Menu.Item>
        <Menu.Divider />
        <Menu.Item key="4">
          <a onClick={() => setFilter("delivered")}>Delivered</a>
        </Menu.Item>
      </Menu>
    );

    const AdminOrder = ({ isVisible, data, id, filter }) => {
      return (
        <div style={{ width: "100%", marginBottom: 12 }}>
          <Row style={{ border: "#f1f1f1 1px solid", padding: 12 }}>
            <Col span={16}>
              <p style={{ display: "block" }} strong>
                {data.displayName}
              </p>
              <p style={{ fontSize: "0.8rem", color: "#707070" }}>
                {data.location}
              </p>
            </Col>
            <Col span={6}>
              <Tag color="green" style={{ float: "right" }}>
                {filter.toUpperCase()}
              </Tag>
            </Col>
            <Col span={2}>
              <Button type="link" color="#707070">
                <DownOutlined />
              </Button>
            </Col>
          </Row>
          <Container
            style={{
              display: isVisible ? "block" : "none",
              background: "#f1f1f1",
            }}
          >
            <Divider orientation="right">Products</Divider>
            {filter == "paid" && data.cart && (
              <>
                {data.cart
                  .filter(
                    (cartItem) =>
                      cartItem.paid == true &&
                      cartItem.packed == false &&
                      cartItem.delivered == false
                  )
                  .map((cartItem) => {
                    return (
                      <Row
                        style={{
                          padding: "12px 0px",
                          borderBottom: "#fff 1px solid",
                        }}
                        key={cartItem.product.id}
                      >
                        <Col span={10}>{cartItem.product.name}</Col>
                        <Col span={2} offset={1}>
                          {cartItem.quantity}
                        </Col>
                        <Col span={1} offset={1}>
                          @
                        </Col>
                        <Col span={3} offset={1}>
                          {cartItem.product.price}
                        </Col>
                        <Col span={4} offset={1}>
                          <strong>
                            {cartItem.product.price * cartItem.quantity}
                          </strong>
                        </Col>
                      </Row>
                    );
                  })}
                <p style={{ width: "100%", textAlign: "right", padding: 12 }}>
                  Tot :{" "}
                  <strong>
                    Ksh.{" "}
                    {getTotal(
                      data.cart
                        ? data.cart.filter(
                            (cartItem) =>
                              cartItem.paid == true &&
                              cartItem.packed == false &&
                              cartItem.delivered == false
                          )
                        : 0
                    )}
                  </strong>
                </p>
                <br />
                <Tag color="green" style={{ float: "right" }}>
                  {"PWNIXBJ67"}
                </Tag>
                <Divider />
                <Checkbox onChange={() => onChangePacked(id)}>Packed</Checkbox>
              </>
            )}
            {filter == "packed" && data.cart && (
              <>
                {data.cart
                  .filter(
                    (cartItem) =>
                      cartItem.paid == true &&
                      cartItem.packed == true &&
                      cartItem.delivered == false
                  )
                  .map((cartItem) => {
                    return (
                      <Row
                        style={{
                          padding: "12px 0px",
                          borderBottom: "#fff 1px solid",
                        }}
                        key={cartItem.product.id}
                      >
                        <Col span={10}>{cartItem.product.name}</Col>
                        <Col span={2} offset={1}>
                          {cartItem.quantity}
                        </Col>
                        <Col span={1} offset={1}>
                          @
                        </Col>
                        <Col span={3} offset={1}>
                          {cartItem.product.price}
                        </Col>
                        <Col span={4} offset={1}>
                          <strong>
                            {cartItem.product.price * cartItem.quantity}
                          </strong>
                        </Col>
                      </Row>
                    );
                  })}
                <Checkbox onChange={() => onChangeDelivered(id)}>
                  Delivered
                </Checkbox>
              </>
            )}
            {filter == "delivered" && data.cart && (
              <>
                {data.cart
                  .filter(
                    (cartItem) =>
                      cartItem.paid == true &&
                      cartItem.packed == true &&
                      cartItem.delivered == true
                  )
                  .map((cartItem) => {
                    return (
                      <Row
                        style={{
                          padding: "12px 0px",
                          borderBottom: "#fff 1px solid",
                        }}
                        key={cartItem.product.id}
                      >
                        <Col span={7}>{cartItem.product.name}</Col>
                        <Col span={2} offset={1}>
                          {cartItem.quantity}
                        </Col>
                        <Col span={1}>@</Col>
                        <Col span={3} offset={1}>
                          {cartItem.product.price}
                        </Col>
                        <Col span={4}>
                          <strong>
                            {cartItem.product.price * cartItem.quantity} /=
                          </strong>
                        </Col>
                        <Col span={5}>
                          <Tag color="yellow">
                            {new Date(cartItem.added)
                              .toLocaleDateString()
                              .slice(0, 5)}
                          </Tag>
                        </Col>
                      </Row>
                    );
                  })}
              </>
            )}
          </Container>
          <Divider />
        </div>
      );
    };

    return (
      <>
        <Row style={{ width: "100%" }}>
          <Col span={22}>
            <Input
              value={keyword}
              placeholder="Search order..."
              suffix={<SearchOutlined />}
              onChange={(e) => setKeyword(e.target.value)}
            />
          </Col>
          <Col span={1}>
            <Dropdown overlay={menuFilterOrders} trigger={["click"]}>
              <Button type="link" style={{ color: "#3F9B42" }}>
                <FilterOutlined />
              </Button>
            </Dropdown>
          </Col>
        </Row>
        <br />
        <div
          style={{
            maxHeight: "calc(100vh - 200px)",
            overflow: "scroll",
            width: "100%",
          }}
        >
          {!users && usersLoading && <p>Loading...</p>}
          {users && users.docs.length < 1 && <p>No orders yet</p>}
          {usersError && <p>Error..</p>}
          {users &&
            users.docs.length > 0 &&
            users.docs
              .filter((user) => {
                if (filter == "paid") {
                  return user
                    .data()
                    .cart.some(
                      (cartItem) =>
                        cartItem.paid == true &&
                        cartItem.packed == false &&
                        cartItem.delivered == false
                    );
                } else if (filter == "packed") {
                  return user
                    .data()
                    .cart.some(
                      (cartItem) =>
                        cartItem.paid == true &&
                        cartItem.packed == true &&
                        cartItem.delivered == false
                    );
                } else if (filter == "delivered") {
                  return user
                    .data()
                    .cart.some(
                      (cartItem) =>
                        cartItem.paid == true &&
                        cartItem.packed == true &&
                        cartItem.delivered == true
                    );
                }
              })
              .map((user, index) => {
                let data = user.data();
                return (
                  <AdminOrder
                    key={index}
                    isVisible={true}
                    data={data}
                    id={user.id}
                    filter={filter}
                  />
                );
              })}
        </div>
      </>
    );
  };

  // const AdminsComponent = () => {
  //   return (
  //     <Container>
  //       {[1, 1, 1].map((admin) => {
  //         return (
  //           <>
  //             <Row>
  //               <Col span={12}>Wilson Kinyanjui</Col>
  //               <Col>
  //                 <a>wilsonk</a>
  //               </Col>
  //             </Row>
  //             <Divider />
  //           </>
  //         );
  //       })}
  //     </Container>
  //   );
  // };

  return (
    <>
      <Container
        style={{
          position: "absolute",
          top: 16,
          width: "95%",
          minHeight: "95vh",
        }}
      >
        <Title level={3} style={{ color: "#1890ff" }}>
          Admin
        </Title>
        <Tabs defaultActiveKey="1">
          {/* <TabPane tab="Dashboard" key="1"></TabPane> */}
          <TabPane tab="Products" key="2">
            <ProductsComponent />
          </TabPane>
          <TabPane tab="Orders" key="3">
            <OrdersComponent />
          </TabPane>
          {/* <TabPane tab="Admins" key="4">
              <AdminsComponent />       
            </TabPane> */}
        </Tabs>
      </Container>
    </>
  );
};
