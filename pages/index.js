import { useState, useEffect, useRef } from "react";

import { db, storage } from "../firebase/clientApp";

import DB_Services from "../firebase/services";
import {
  query,
  collection,
  orderBy,
  limit,
  getDocs,
  startAfter,
  doc,
} from "firebase/firestore";
import { useDocument } from "react-firebase-hooks/firestore";
import {
  Container,
  Product,
  Button,
  Title,
  Search,
  Category,
  Text,
  Flex,
  Page,
  SkeletonCustom,
} from "../components";
import Mark from "mark.js";

import {
  Badge,
  Avatar,
  Carousel,
  message,
  Dropdown,
  Skeleton,
  Spin,
} from "antd";
import { ShoppingOutlined, UserOutlined } from "@ant-design/icons/lib/icons";

import { useAuth } from "../context";
import { useRouter } from "next/router";

export default function Home() {
  const { authUser, loading } = useAuth();
  const router = useRouter();

  const [uda, setUDA] = useState(false);
  const [signedIn, setSignedIn] = useState(true);
  const [highlighted, setHighlighted] = useState("");
  const [keyword, setKeyword] = useState("");
  const [id, setId] = useState(null);
  const [prods, setProds] = useState([]);
  const [lastKey, setLastKey] = useState("");
  const [loadingMore, setLoadingMore] = useState(false);

  const scrollDiv = useRef();

  const [user, userLoading, userError] = useDocument(
    doc(db, "users", authUser ? authUser.uid : "unfindalble"),
    {}
  );

  useEffect(() => {
    if (user && typeof user.data() !== "undefined") {
      setUDA(true);
    }
  }, [user]);

  useEffect(async () => {
    let data = query(
      collection(db, "products"),
      orderBy("added", "desc"),
      limit(15)
    );

    const snapShot = await getDocs(data);

    let _data_ = [];

    snapShot.docs.forEach((doc) => {
      _data_[_data_.length] = { id: doc.id, data: doc.data() };
    });

    setLastKey(snapShot.docs[snapShot.docs.length - 1]);
    setProds(_data_);
  }, []);

  useEffect(() => {
    let markInstance = new Mark(document.querySelector("#search-node"));
    markInstance.unmark({
      done: () => {
        markInstance.mark(keyword);
      },
    });
  }, [keyword]);

  const getProducts = async (doc) => {
    setLoadingMore(true);

    let data = query(
      collection(db, "products"),
      orderBy("added", "desc"),
      startAfter(doc),
      limit(15)
    );

    const snapShot = await getDocs(data);

    setLastKey(snapShot.docs[snapShot.docs.length - 1]);

    let _data_ = [];

    snapShot.docs.forEach((doc) => {
      _data_[_data_.length] = { id: doc.id, data: doc.data() };
    });

    setProds(prods.concat(_data_));
  };

  const fetchMore = () => {
    if (
      scrollDiv.current.offsetHeight + scrollDiv.current.scrollTop >=
      scrollDiv.current.scrollHeight
    ) {
      getProducts(lastKey).then(() => setLoadingMore(false));
    }
  };

  const addToCart = (product) => {
    console.log(authUser);
    if (authUser == null) {
      router.push("/login");
    } else {
      setId(product.id);

      DB_Services.addToCart({
        product: {
          id: product.id,
          name: product.data.name,
          image: product.data.image,
          price:
            DB_Services.verifyOnOffer(
              product.data.start_date,
              product.data.end_date
            ) && product.data.offer_price
              ? product.data.offer_price
              : product.data.price,
        },
        user: authUser.uid,
      })
        .then(() => {
          message.success(`'${product.data.name}' successfully added to cart`);
          setId(null);
        })
        .catch((err) => {
          message.error("Failed");
          setId(null);
        });
    }
  };

  const increment = (id) => {
    DB_Services.incrementCartProduct({ id, user: authUser.uid }).then((val) =>
      console.log(val)
    );
  };

  const decrement = (id, name) => {
    if (
      user != undefined &&
      user.data().cart.filter((cartItem) => cartItem.product.id == id)[0]
        .quantity == 1
    ) {
      DB_Services.removeFromCart({ id, user: authUser.uid }).then((val) => {
        console.log(val);
        message.success(`'${name}' removed from cart`);
      });
    } else {
      DB_Services.decrementCartProduct({ id, user: authUser.uid }).then((val) =>
        console.log(val)
      );
    }
  };

  const removeFromCart = (id, name) => {
    DB_Services.removeFromCart({ id, user: authUser.uid }).then((val) => {
      console.log(val);
      message.success(`'${name}' removed from cart`);
    });
  };

  const results = (
    <div
      style={{ maxHeight: 200, width: "100%", background: "#fff" }}
      id="search-node"
    >
      {/* {products &&
        products.docs
          .filter((product) =>
            product.data().name.toLowerCase().includes(keyword.toLowerCase())
          )
          .map((product) => (
            <a
              href={`#${product.id}`}
              onClick={() => {
                setHighlighted(product.id);
                setKeyword("");
              }}
              key={product.id}
              style={{ display: "block", margin: "12px", width: "100%" }}
            >
              {product.data().name}
            </a>
          ))}
      {!products && [1, 2, 3, 4, 5].map((el) => <Skeleton.Button key={el} />)} */}
    </div>
  );

  return (
    <Container>
      {/* Header */}
      <>
        <div
          style={{
            background: "#fff",
            display: "flex",
            position: "fixed",
            display: "flex",
            top: 0,
            width: "100%",
            padding: "12px 0px",
            zIndex: 3,
          }}
        >
          <Dropdown
            overlay={results}
            placement="bottomCenter"
            visible={keyword ? true : false}
          >
            <Search
              rounded
              placeholder="Search eggs , ketchup..."
              value={keyword}
              onChange={(e) => setKeyword(e.target.value)}
            />
          </Dropdown>

          <div style={{ height: "inherit", paddingTop: 6, display: "flex" }}>
            <button
              style={{
                height: 40,
                margin: 0,
                outline: "none",
                border: "none",
                background: "white",
                display: "inline",
                top: 12,
              }}
              onClick={() => {
                if (!uda) {
                  router.push("/login");
                } else {
                  router.push("/basket");
                }
              }}
            >
              <Badge
                color="#3F9B42"
                count={
                  uda
                    ? user
                        .data()
                        .cart.filter(
                          (cartItem) =>
                            cartItem.paid == false &&
                            cartItem.deliver_first == false
                        ).length
                    : 0
                }
              >
                <ShoppingOutlined
                  style={{ fontSize: "1.5rem", color: "#707070" }}
                />
              </Badge>
            </button>
            <button
              style={{
                height: 40,
                margin: 0,
                outline: "none",
                border: "none",
                background: "white",
                display: "inline",
                top: 12,
              }}
              onClick={() => {
                if (!uda) {
                  router.push("/login");
                } else {
                  router.push("/account");
                }
              }}
            >
              <Avatar
                style={{ backgroundColor: "#3F9B42" }}
                icon={<UserOutlined />}
              />
            </button>
          </div>
        </div>
        <br />
      </>

      <div
        ref={scrollDiv}
        onScroll={fetchMore}
        style={{
          position: "absolute",
          top: 78,
          width: "100%",
          maxHeight: "200vh",
          overflow: "auto",
        }}
      >
        {/* <Offers />
        <Carousel autoplay>
          <div style={{ height: 160, width: "calc(100vw - 32px)" }}>
            <img
              style={{ width: "100%", objectFit: "cover", height: "160px" }}
              src="/slider/slider-1.png"
            />
          </div>
          <div style={{ height: 160, width: "calc(100vw - 32px)" }}>
            <p>Another promotion here</p>
          </div>
        </Carousel> */}

        {/* Categories */}
        <>
          <Title text="Categories" />
          <br />

          <div style={{ display: "flex", maxWidth: "100%", flexWrap: "wrap" }}>
            {[
              "sodas",
              "juices",
              "medicine",
              "snacks",
              "legumes & flour",
              "breads & cakes",
              "spices & seasoning",
              "plastics",
              "milk products",
              "meat products",
              "frozen food",
              "electricals & electronics",
              "clothing",
              "dental products",
              "gas & gas products",
              "stationary",
              "fruits & vegetables",
              "oil & skin products",
              "hardware products",
              "other",
            ].map((cat, inx) => (
              <Category
                key={inx}
                icon={
                  cat == "sodas"
                    ? "/categories/soda.png"
                    : cat == "juices"
                    ? "/categories/juice.png"
                    : cat == "medicine"
                    ? "/categories/medicine.png"
                    : cat == "snacks"
                    ? "/categories/snacks.png"
                    : cat == "legumes & flour"
                    ? "/categories/flour.png"
                    : cat == "breads & cakes"
                    ? "/categories/bread.png"
                    : cat == "spices & seasoning"
                    ? "/categories/seasoning.png"
                    : cat == "plastics"
                    ? "/categories/plastics.png"
                    : cat == "milk products"
                    ? "/categories/milk.png"
                    : cat == "meat products"
                    ? "/categories/meat.png"
                    : cat == "frozen food"
                    ? "/categories/frozen.png"
                    : cat == "electricals & electronics"
                    ? "/categories/electronics.png"
                    : cat == "clothing"
                    ? "/categories/clothing.png"
                    : cat == "dental products"
                    ? "/categories/dental.png"
                    : cat == "gas & gas products"
                    ? "/categories/gas.png"
                    : cat == "stationary"
                    ? "/categories/stationery.png"
                    : cat == "fruits & vegetables"
                    ? "/categories/fruits.png"
                    : cat == "oil & skin products"
                    ? "/categories/skincare.png"
                    : "/categories/hardware.png"
                }
                rounded
                label={
                  cat.charAt(0).toUpperCase() + cat.substring(1, cat.length)
                }
              />
            ))}
          </div>
          <br />
        </>
        {/* Recommended */}
        <>
          <Title text="Recommended" />
          <br />

          {prods.length < 0 && (
            <Flex scrollY>
              {[1, 2, 3, 5, 6, 7, 8, 9, 4].map((el) => (
                <SkeletonCustom key={el} />
              ))}
            </Flex>
          )}

          <Flex scrollY>
            {prods.length > 0 &&
              uda &&
              prods.map((product) => (
                <Product
                  on_offer={
                    product.data.start_date && product.data.end_date
                      ? DB_Services.verifyOnOffer(
                          product.data.start_date,
                          product.data.end_date
                        )
                      : false
                  }
                  offer_price={product.data.offer_price}
                  key={product.id}
                  image={product.data.image}
                  loading={id == product.id ? true : false}
                  add={() => addToCart(product)}
                  rounded
                  highlighted={highlighted == product.id ? true : false}
                  basket={
                    user
                      .data()
                      .cart.filter(
                        (cartItem) =>
                          cartItem.product.id == product.id &&
                          cartItem.paid == false &&
                          cartItem.deliver_first == false
                      ).length > 0
                      ? true
                      : false
                  }
                  quantity={
                    user
                      .data()
                      .cart.filter(
                        (cartItem) =>
                          cartItem.product.id == product.id &&
                          cartItem.paid == false &&
                          cartItem.deliver_first == false
                      ).length > 0
                      ? user
                          .data()
                          .cart.filter(
                            (cartItem) =>
                              cartItem.product.id == product.id &&
                              cartItem.paid == false &&
                              cartItem.deliver_first == false
                          )[0].quantity
                      : 0
                  }
                  name={product.data.name}
                  price={product.data.price}
                  id={product.id}
                  remove={(id) => {
                    if (
                      user
                        .data()
                        .cart.filter(
                          (cartItem) =>
                            cartItem.product.id == id &&
                            cartItem.paid == false &&
                            cartItem.deliver_first == false
                        ).length > 0
                    ) {
                      removeFromCart(id, product.data.name);
                    }
                  }}
                  increment={(id) => increment(id, product.data.name)}
                  decrement={(id) => decrement(id, product.data.name)}
                />
              ))}
          </Flex>

          <Flex scrollY>
            {prods.length > 0 &&
              (!uda || userLoading || userError) &&
              prods.map((product) => (
                <Product
                  on_offer={
                    product.data.start_date && product.data.end_date
                      ? DB_Services.verifyOnOffer(
                          product.data.start_date,
                          product.data.end_date
                        )
                      : false
                  }
                  offer_price={product.data.offer_price}
                  key={product.id}
                  image={product.data.image}
                  loading={id == product.id ? true : false}
                  add={() => addToCart(product)}
                  rounded
                  highlighted={highlighted == product.id ? true : false}
                  name={product.data.name}
                  price={product.data.price}
                  id={product.id}
                />
              ))}
          </Flex>

          {loadingMore && (
            <Spin
              style={{
                position: "absolute",
                bottom: "0.5rem",
                left: "50%",
                transform: "translateY(-50%)",
              }}
            />
          )}
        </>
      </div>
    </Container>
  );
}
