import {
  Container,
  Product,
  Button,
  Flex,
  Page,
  SkeletonCustom,
} from "../components";
import { Button as ButtonAntd, Divider, message } from "antd";
import { useRouter } from "next/router";
import DB_Services from "../firebase/services";
import { useAuth } from "../context";

import { db } from "../firebase/clientApp";

import { doc } from "firebase/firestore";
import { useDocument } from "react-firebase-hooks/firestore";

export default function Basket() {
  const router = useRouter();
  const { authUser, loading } = useAuth();

  const [user, userLoading, userError] = useDocument(
    doc(db, "users", authUser ? authUser.uid : "unfindalble"),
    {}
  );

  if (!authUser || loading)
    return (
      <Flex scrollY>
        {[1, 2, 3, 4, 5, 6, 7, 8, 9, 10].map((el) => (
          <SkeletonCustom key={el} />
        ))}
      </Flex>
    );

  const Bill = ({ total }) => {
    return (
      <p
        style={{
          color: "rgb(229, 224, 85)",
          lineHeight: 2.5,
          fontFamily: "Metropolis-Regular",
        }}
      >
        {total}
      </p>
    );
  };

  const getTotal = () => {
    let tot = 0;
    user.data().cart.forEach((cartItem) => {
      if (cartItem.paid == false && cartItem.deliver_first == false) {
        tot += cartItem.quantity * cartItem.product.price;
      }
    });
    return tot;
  };

  const increment = (id) => {
    DB_Services.incrementCartProduct({ id, user: authUser.uid }).then((val) =>
      console.log(val)
    );
  };

  const decrement = (id, name) => {
    if (
      user.data().cart.filter((cartItem) => cartItem.product.id == id)[0]
        .quantity == 1
    ) {
      DB_Services.removeFromCart({ id, user: authUser.uid }).then((val) => {
        console.log(val);
        message.success(`'${name}' removed from cart`);
      });
    } else {
      DB_Services.decrementCartProduct({ id, user: authUser.uid }).then((val) =>
        console.log(val)
      );
    }
  };

  const removeFromCart = (id, name) => {
    DB_Services.removeFromCart({ id, user: authUser.uid }).then((val) => {
      console.log(val);
      message.success(`'${name}' removed from cart`);
    });
  };

  return (
    <Page
      title="Basket"
      extra={<Bill total={`Ksh. ${user ? getTotal() : 0}`} />}
    >
      <Container style={{ position: "absolute", top: 56, minWidth: "95vw" }}>
        {userError && <p>Error...</p>}
        {userLoading && (
          <Flex scrollY style={{ paddingBottom: "5rem" }}>
            {[1, 2, 3, 4, 5, 6, 7, 8, 9, 10].map((el) => (
              <SkeletonCustom key={el} />
            ))}
          </Flex>
        )}

        {user &&
        user
          .data()
          .cart.filter(
            (cartItem) =>
              cartItem.paid == false && cartItem.deliver_first == false
          ).length > 0 ? (
          <>
            <Flex scrollY style={{ paddingBottom: "5rem" }}>
              {user
                .data()
                .cart.filter(
                  (cartItem) =>
                    cartItem.paid == false && cartItem.deliver_first == false
                )
                .map((order, index) => (
                  <Product
                    key={index}
                    name={order.product.name}
                    price={order.product.price}
                    quantity={order.quantity}
                    rounded
                    image={order.product.image}
                    basket
                    id={order.product.id}
                    remove={(id) => {
                      removeFromCart(id, order.product.name);
                    }}
                    increment={(id) => increment(id, order.product.name)}
                    decrement={(id) => {
                      decrement(id, order.product.name);
                    }}
                  />
                ))}
            </Flex>

            {/* Awaiting delivery */}
            {user
              .data()
              .cart.filter(
                (cartItem) =>
                  cartItem.paid == false && cartItem.deliver_first == true
              ).length > 0 && (
              <>
                <Divider orientation="right">Awaiting delivery</Divider>
                <Flex scrollY style={{ paddingBottom: "5rem" }}>
                  {user
                    .data()
                    .cart.filter(
                      (cartItem) =>
                        cartItem.paid == false && cartItem.deliver_first == true
                    )
                    .map((order, index) => (
                      <Product
                        key={index}
                        name={order.product.name}
                        price={order.product.price}
                        quantity={order.quantity}
                        rounded
                        image={order.product.image}
                        old
                        id={order.product.id}
                      />
                    ))}
                </Flex>
              </>
            )}

            {/* Older purchases */}
            {user.data().cart.filter((cartItem) => cartItem.paid == true)
              .length > 0 && (
              <>
                <Divider orientation="right">Older purchases</Divider>
                <Flex scrollY style={{ paddingBottom: "5rem" }}>
                  {user
                    .data()
                    .cart.filter((cartItem) => cartItem.paid == true)
                    .map((order, index) => (
                      <Product
                        key={index}
                        name={order.product.name}
                        price={order.product.price}
                        quantity={order.quantity}
                        rounded
                        basket={false}
                        image={order.product.image}
                        old
                        id={order.product.id}
                      />
                    ))}
                </Flex>
              </>
            )}

            <Button
              onClick={() => router.push("/checkout")}
              rounded
              label="Checkout"
              yellow
              width="90%"
              large
              style={{ position: "fixed", bottom: "3rem" }}
            />
          </>
        ) : (
          user &&
          user
            .data()
            .cart.filter(
              (cartItem) =>
                cartItem.paid == false && cartItem.deliver_first == false
            ).length < 1 && (
            <>
              <img
                src="https://img.icons8.com/dusk/100/000000/shopping-basket-2.png"
                style={{
                  position: "absolute",
                  top: "20vh",
                  left: "50%",
                  transform: "translateX(calc(-50% + 8px))",
                }}
              />
              <p
                style={{
                  color: "#3F9B42",
                  position: "absolute",
                  transform: "translateX(calc(-50% + 8px))",
                  letterSpacing: "-0.05rem",
                  top: "calc(20vh + 120px)",
                  left: "50%",
                  fontSize: "1.5rem",
                }}
              >
                Basket empty!
              </p>
              <ButtonAntd
                style={{
                  width: "100%",
                  color: "black",
                  position: "absolute",
                  transform: "translateX(calc(-50% + 8px))",
                  top: "calc(20vh + 180px)",
                  left: "50%",
                }}
                onClick={() => router.push("/")}
                type="link"
              >
                <p style={{ textDecoration: "underline", fontWeight: "400" }}>
                  {" "}
                  Start shopping
                </p>
              </ButtonAntd>
              <div style={{ width: "100%", marginTop: "75vh" }}>
                {/* Awaiting delivery */}
                {user
                  .data()
                  .cart.filter(
                    (cartItem) =>
                      cartItem.paid == false && cartItem.deliver_first == true
                  ).length > 0 && (
                  <>
                    <Divider orientation="right">Awaiting delivery</Divider>
                    <Flex scrollY style={{ paddingBottom: "2rem" }}>
                      {user
                        .data()
                        .cart.filter(
                          (cartItem) =>
                            cartItem.paid == false &&
                            cartItem.deliver_first == true
                        )
                        .map((order, index) => (
                          <Product
                            key={index}
                            name={order.product.name}
                            price={order.product.price}
                            quantity={order.quantity}
                            rounded
                            image={order.product.image}
                            old
                            id={order.product.id}
                          />
                        ))}
                    </Flex>
                  </>
                )}

                {/* Older purchases */}
                {user.data().cart.filter((cartItem) => cartItem.paid == true)
                  .length > 0 && (
                  <>
                    <Divider orientation="right">Older purchases</Divider>
                    <Flex scrollY style={{ paddingBottom: "5rem" }}>
                      {user
                        .data()
                        .cart.filter((cartItem) => cartItem.paid == true)
                        .map((order, index) => (
                          <Product
                            key={index}
                            name={order.product.name}
                            price={order.product.price}
                            quantity={order.quantity}
                            rounded
                            image={order.product.image}
                            old
                            id={order.product.id}
                          />
                        ))}
                    </Flex>
                  </>
                )}
              </div>
            </>
          )
        )}
      </Container>
    </Page>
  );
}
