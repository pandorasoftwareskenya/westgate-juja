import Title from "./Title";
import Button from "./Button";
// import Image from "next/image";
import {
  Image,
  Button as ButtonAntd,
  Skeleton,
  Tag,
  Typography,
  Tooltip,
} from "antd";

import { PlusOutlined, MinusOutlined, CloseOutlined } from "@ant-design/icons";

const { Paragraph } = Typography;

export default function Product({
  loading,
  add,
  id,
  name,
  price,
  quantity,
  image,
  remove,
  increment,
  decrement,
  basket,
  rounded,
  on_offer,
  offer_price,
  old,
  highlighted,
}) {
  return (
    <section
      id={id}
      style={{
        width: "calc(50vw - 32px)",
        maxWidth: 200,
        margin: "0px 16px 16px 5px",
        background: old || highlighted ? "rgba(229,224,85,0.2)" : "#f1f1f1",
        borderRadius: rounded ? "8px" : "0px",
        position: "relative",
      }}
    >
      {basket ? (
        <Button
          onClick={() => remove(id)}
          label={
            <img
              src={"/cross.svg"}
              style={{ height: 32, width: 32, objectFit: "cover", zIndex: 3 }}
            />
          }
          style={{
            zIndex: 2,
            position: "absolute",
            top: 3,
            right: 12,
            height: 32,

            background: "transparent",
          }}
          width={32}
        />
      ) : null}

      {on_offer && (
        <Tag
          color="green"
          style={{
            zIndex: 2,
            position: "absolute",
            top: 0,
            left: 0,
          }}
        >
          OFFER!
        </Tag>
      )}

      <div style={{ padding: 12 }}>
        {image ? (
          <Image
            style={{
              width: `calc(50vw - 56px)`,
              height: `calc(50vw - 56px)`,
              objectFit: "cover",
              marginBottom: 12,
              width: "100%",
              maxWidth: "calc(200px - 24px)",
              maxHeight: "calc(200px - 24px)",
            }}
            src={image}
            loading="eager"
          />
        ) : (
          <Skeleton.Button
            style={{
              width: `calc(50vw - 56px)`,
              height: `calc(50vw - 56px)`,
              marginBottom: 12,
            }}
            active
          />
        )}
        <Tooltip title={name} color="#3F9B42" trigger="click">
          <Paragraph
            ellipsis={{ rows: 2, expandable: false }}
            style={{
              color: old ? "#3F9B42" : "#707070",
              width: `calc(50vw - 60px)`,
              maxWidth: "calc(200px - 24px)",
              height: 46,
            }}
          >
            {name}
          </Paragraph>
        </Tooltip>
        {on_offer && offer_price && (
          <span
            style={{
              display: "flex",
              margin: "0.4rem 0rem",
              display: "flex",
              justifyContent: "space-between",
              width: "100%",
            }}
          >
            <Title text={`Ksh. ${offer_price}`} style={{ margin: 0 }} />
            <h3
              style={{
                textDecoration: "line-through",
                color: "gray",
                margin: 0,
                lineHeight: 2,
                fontSize: "0.8rem",
                fontWeight: "400",
              }}
            >
              {price}
            </h3>
          </span>
        )}

        {!on_offer && <Title text={`Ksh. ${price}`} yellow={old} />}

        {old && (
          <div
            style={{ display: "flex", justifyContent: "center", width: "100%" }}
          >
            <Tag color="#E5E055">{quantity} Products</Tag>
            {/* <Title
              style={{ lineHeight: 1, color: "#3F9B42", textAlign: "center" }}
              text={quantity}
            /> */}
          </div>
        )}

        {basket && (
          <div
            style={{
              display: "flex",
              justifyContent: "space-between",
            }}
          >
            <div style={{ width: 36 }}>
              <Button
                rounded
                label={<MinusOutlined />}
                style={{ height: 36 }}
                full
                onClick={() => decrement(id)}
              />
            </div>
            <div>
              <Title
                style={{ lineHeight: 1, color: "#707070" }}
                text={quantity}
              />
            </div>
            <div style={{ width: 36 }}>
              <Button
                rounded
                label={<PlusOutlined />}
                style={{ height: 36 }}
                full
                onClick={() => increment(id)}
              />
            </div>
          </div>
        )}

        {old ? null : basket ? null : (
          <ButtonAntd
            loading={loading}
            onClick={add}
            style={{
              fontFamily: "Metropolis-Regular",
              display: "block",
              fontSize: "1rem",

              backgroundColor: "#3F9B42",
              fontWeight: "900",
              textTransform: "uppercase",
              color: "#fff",
              border: "none",
              borderRadius: "8px",
              width: "100%",
              height: 36,
            }}
            full
          >
            {loading ? "Adding" : "Add"}
          </ButtonAntd>
        )}
      </div>
    </section>
  );
}
