import { Router } from "next/router";
import Title from "./Title";
import { Input, Dropdown } from "antd";

import { useRouter } from "next/router";
import { SearchOutlined } from "@ant-design/icons";
import { useEffect, useState } from "react";

export default function Page({
  children,
  title,
  style,
  extra,
  category,
  badge,
  getKeyword,
}) {
  const router = useRouter();
  const [dropdown, setDropdown] = useState(false);

  const search = (
    <input
      style={{
        padding: 8,
        margin: 8,
        border: "none",
        border: "#3F9B42 1px solid",
        outline: "none",
      }}
      allowClear
      onChange={(e) => getKeyword(e.target.value)}
      placeholder="Search product ..."
    />
  );

  return (
    <div
      style={{
        width: "100vw",
        maxWidth: "100vw",
        minHeight: "100vh",
        height: "100vh",
        overflowX: "hidden",
        ...style,
      }}
    >
      {title ? (
        <div
          style={{
            background: "#3F9B42",
            height: 56,
            width: "100%",
            padding: 12,
            position: "fixed",
            zIndex: 3,
            display: "flex",
            justifyContent: "space-between",
          }}
        >
          <div style={{ display: "flex" }}>
            <button
              style={{
                background: "transparent",
                margin: 0,
                outline: "none",
                border: "none",
                marginRight: 16,
              }}
              onClick={() => router.back()}
            >
              <img
                src="/back.svg"
                style={{
                  height: 16,
                  objectFit: "cover",
                  transform: "rotate(180deg)",
                }}
              />
            </button>
            <p
              style={{
                color: "#fff",
                textTransform: "capitalize",
                lineHeight: 1.9,
                fontSize: "1.2rem",
                fontFamily: "Metropolis-SemiBold",
              }}
            >
              {title}
            </p>
          </div>
          {category && (
            <div style={{ display: "flex" }}>
              <Dropdown
                overlay={search}
                placement="bottomCenter"
                visible={dropdown ? true : false}
              >
                <button
                  style={{
                    height: 40,
                    margin: 0,
                    outline: "none",
                    border: "none",
                    background: "transparent",
                    display: "inline",
                    top: 12,
                    marginRight: 8,
                  }}
                  onClick={() => setDropdown(!dropdown)}
                >
                  <SearchOutlined
                    style={{ color: "white", fontSize: "1.2rem" }}
                  />
                </button>
              </Dropdown>
              {badge}
            </div>
          )}

          {extra ? extra : null}
        </div>
      ) : null}
      {children}
    </div>
  );
}
