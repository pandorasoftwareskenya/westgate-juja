import {
  collection,
  addDoc,
  doc,
  increment,
  deleteDoc,
  updateDoc,
  setDoc,
  arrayUnion,
  arrayRemove,
  query,
  where,
  getDoc,
  getDocs,
} from "firebase/firestore";
import { ref, uploadString, getDownloadURL } from "firebase/storage";
import { db, storage } from "./clientApp";

const productsRef = collection(db, "products");
const ordersRef = collection(db, "orders");
const usersRef = collection(db, "users");

const uploadProductImage = (name, file) => {
  console.log({
    name,
    file,
  });
  return new Promise((resolve, reject) => {
    const storageRef = ref(storage, "products/" + `${name}`);

    fetch(file)
      .then((res) => res.blob())
      .then((blob) => {
        let reader = new FileReader();
        reader.readAsDataURL(blob);
        reader.onloadend = function () {
          let base64data = reader.result;
          console.log(base64data);
          uploadString(storageRef, base64data, "data_url")
            .then((snapshot) => {
              if (snapshot) {
                console.log("Started upload");
                const progress =
                  (snapshot.bytesTransferred / snapshot.totalBytes) * 100;

                switch (snapshot.state) {
                  case "paused":
                    console.log("Upload is paused");
                    break;
                  case "running":
                    console.log("Upload is running");
                    break;
                }

                return snapshot.ref;
              }
            })
            .then((ref) => {
              console.log("Ref generated");
              getDownloadURL(ref)
                .then((downloadURL) => {
                  resolve(downloadURL);
                  console.log("File available at", downloadURL);
                })
                .catch((err) => reject(err));
            });
        };
      });
  });
};

const createProduct = async (args) => {
  const docRef = await addDoc(productsRef, {
    name: args.name,
    price: args.price,
    quantity: args.quantity,
    category: args.category,
    image: args.image,
    offer_price: null,
    on_offer: false,
    added: Date.now(),
  });

  return docRef;
};

const createUser = async (args) => {
  let docRef;
  if (args.uid == null) {
    docRef = await addDoc(collection(db, "users"), {
      displayName: args.displayName,
      email: typeof args.email != "undefined" ? args.email : null,
      phoneNumber: args.phoneNumber,
      photo: args.photo == null ? null : args.photo,
      cart: [],
    });
    return docRef.id;
  } else {
    await setDoc(doc(db, "users", args.uid), {
      displayName: args.displayName,
      email: typeof args.email != "undefined" ? args.email : null,
      phoneNumber: args.phoneNumber,
      photo: args.photo == null ? null : args.photo,
      cart: [],
    });
  }
};

const updateUser = async (update, id) => {
  await updateDoc(doc(db, "users", id), update);
};

const addOffer = async (args) => {
  const { offer_price, startDate, endDate } = args;
  let data = {
    offer_price,
    start_date: startDate,
    end_date: endDate,
  };
  await updateDoc(doc(db, "products", `${args.id}`), data);
};

const updateProduct = async (args) => {
  const { id, name, price, category, quantity } = args;
  let data = {
    name,
    price,
    category,
    quantity,
  };
  await updateDoc(doc(db, "products", `${id}`), data);
};

const deleteProduct = async (args) => {
  await deleteDoc(doc(db, "products", args.id));
};

const addToCart = async (args) => {
  const { product, user } = args;
  let itemMeta = {
    added: Date.now(),
    delivered: false,
    packed: false,
    paid: false,
    product,
    quantity: 1,
    deliver_first: false,
  };
  await updateDoc(doc(db, "users", `${user}`), {
    cart: arrayUnion(itemMeta),
  });
};

const setDeliverFirst = async (args) => {
  let docSnap = await getDoc(doc(db, "users", args.user));

  return new Promise((resolve, reject) => {
    if (docSnap.exists()) {
      let new_cart = [
        ...docSnap
          .data()
          .cart.filter((cartItem) => !args.ids.includes(cartItem.product.id)),
      ];

      docSnap
        .data()
        .cart.filter((cartItem) => args.ids.includes(cartItem.product.id))
        .forEach((cartItem) => {
          new_cart.push({
            delivered: cartItem.delivered,
            added: cartItem.added,
            packed: cartItem.packed,
            paid: cartItem.paid,
            product: cartItem.product,
            quantity: cartItem.quantity,
            deliver_first: true,
          });
        });

      console.log(new_cart);

      updateDoc(doc(db, "users", args.user), {
        cart: new_cart,
      })
        .then(() => resolve(true))
        .catch(() => resolve(false));
    } else {
      resolve(false);
    }
  });
};

const checkPhone = async (args) => {
  return new Promise((resolve, reject) => {
    console.log(args.phoneNumber);
    let data = query(
      collection(db, "users"),
      where("phoneNumber", "==", args.phoneNumber)
    );

    getDocs(data).then((snapShot) => {
      if (snapShot.docs.length == 1) {
        resolve({
          uid: snapShot.docs[0].id,
          email: snapShot.docs[0].data().email,
          displayName: snapShot.docs[0].data().displayName,
          phoneNumber: snapShot.docs[0].data().phoneNumber,
          photo: snapShot.docs[0].data().photoURL,
        });
      } else if (snapShot.docs.length > 1) {
        let _data_ = [];
        snapShot.docs.forEach((snap) => {
          _data_[_data_.length] = {
            uid: snap.id,
            email: snap.data().email,
            displayName: snap.data().displayName,
            phoneNumber: snap.data().phoneNumber,
            photo: snap.data().photoURL,
          };
        });
        resolve(_data_);
      } else {
        resolve(null);
      }
    });
  });
};

//obsolette
const createOrder = async (args) => {
  const docRef = await addDoc(ordersRef, {
    added: Date.now(),
    delivered: false,
    packed: false,
    paid: false,
    product: args.product,
    quantity: 1,
    user: args.user,
  });

  console.log(docRef);

  return docRef;
};

const decrementCartProduct = async (args) => {
  let docSnap = await getDoc(doc(db, "users", args.user));

  return new Promise((resolve, reject) => {
    if (docSnap.exists()) {
      let new_cart = [
        ...docSnap
          .data()
          .cart.filter(
            (cartItem) =>
              cartItem.product.id !== args.id || cartItem.paid == true
          ),
        {
          delivered: docSnap
            .data()
            .cart.filter(
              (cartItem) =>
                cartItem.product.id == args.id && cartItem.paid == false
            )[0].delivered,
          added: docSnap
            .data()
            .cart.filter(
              (cartItem) =>
                cartItem.product.id == args.id && cartItem.paid == false
            )[0].added,
          packed: docSnap
            .data()
            .cart.filter(
              (cartItem) =>
                cartItem.product.id == args.id && cartItem.paid == false
            )[0].packed,
          paid: docSnap
            .data()
            .cart.filter(
              (cartItem) =>
                cartItem.product.id == args.id && cartItem.paid == false
            )[0].paid,
          product: docSnap
            .data()
            .cart.filter(
              (cartItem) =>
                cartItem.product.id == args.id && cartItem.paid == false
            )[0].product,
          deliver_first: docSnap
            .data()
            .cart.filter(
              (cartItem) =>
                cartItem.product.id == args.id && cartItem.paid == false
            )[0].deliver_first,
          quantity:
            docSnap
              .data()
              .cart.filter(
                (cartItem) =>
                  cartItem.product.id == args.id && cartItem.paid == false
              )[0].quantity - 1,
        },
      ];

      updateDoc(doc(db, "users", args.user), {
        cart: new_cart,
      })
        .then(() => resolve(true))
        .catch(() => resolve(false));
    } else {
      resolve(false);
    }
  });
};

const incrementCartProduct = async (args) => {
  let docSnap = await getDoc(doc(db, "users", args.user));

  return new Promise((resolve, reject) => {
    if (docSnap.exists()) {
      let new_cart = [
        ...docSnap
          .data()
          .cart.filter(
            (cartItem) =>
              cartItem.product.id !== args.id || cartItem.paid == true
          ),
        {
          delivered: docSnap
            .data()
            .cart.filter(
              (cartItem) =>
                cartItem.product.id == args.id && cartItem.paid == false
            )[0].delivered,
          added: docSnap
            .data()
            .cart.filter(
              (cartItem) =>
                cartItem.product.id == args.id && cartItem.paid == false
            )[0].added,
          packed: docSnap
            .data()
            .cart.filter(
              (cartItem) =>
                cartItem.product.id == args.id && cartItem.paid == false
            )[0].packed,
          paid: docSnap
            .data()
            .cart.filter(
              (cartItem) =>
                cartItem.product.id == args.id && cartItem.paid == false
            )[0].paid,
          product: docSnap
            .data()
            .cart.filter(
              (cartItem) =>
                cartItem.product.id == args.id && cartItem.paid == false
            )[0].product,
          deliver_first: docSnap
            .data()
            .cart.filter(
              (cartItem) =>
                cartItem.product.id == args.id && cartItem.paid == false
            )[0].deliver_first,
          quantity:
            docSnap
              .data()
              .cart.filter(
                (cartItem) =>
                  cartItem.product.id == args.id && cartItem.paid == false
              )[0].quantity + 1,
        },
      ];

      updateDoc(doc(db, "users", args.user), {
        cart: new_cart,
      })
        .then(() => resolve(true))
        .catch(() => resolve(false));
    } else {
      resolve(false);
    }
  });
};

//obsolette
const incrementOrder = async (args) => {
  const docRef = await updateDoc(doc(db, "orders", args.id), {
    quantity: increment(1),
  });
  return docRef;
};

//obsolette
const decrementOrder = async (args) => {
  const docRef = await updateDoc(doc(db, "orders", args.id), {
    quantity: increment(-1),
  });
  return docRef;
};

//obsolette
const deleteOrder = async (args) => {
  const docRef = await deleteDoc(doc(db, "orders", args.id));
  return docRef;
};

const markPacked = async (args) => {
  let docSnap = await getDoc(doc(db, "users", args.user));

  return new Promise((resolve, reject) => {
    if (docSnap.exists()) {
      let newCart = [];

      for (let i = 0; i < docSnap.data().cart.length; i++) {
        let cartItem = {};
        if (
          docSnap.data().cart[i].paid == true &&
          docSnap.data().cart[i].packed == false &&
          docSnap.data().cart[i].delivered == false
        ) {
          cartItem.product = docSnap.data().cart[i].product;
          cartItem.added = docSnap.data().cart[i].added;
          cartItem.quantity = docSnap.data().cart[i].quantity;
          cartItem.paid = docSnap.data().cart[i].paid;
          cartItem.packed = true;
          cartItem.delivered = false;
          newCart[i] = cartItem;
        } else {
          cartItem.product = docSnap.data().cart[i].product;
          cartItem.added = docSnap.data().cart[i].added;
          cartItem.quantity = docSnap.data().cart[i].quantity;
          cartItem.paid = docSnap.data().cart[i].paid;
          cartItem.packed = docSnap.data().cart[i].packed;
          cartItem.delivered = docSnap.data().cart[i].delivered;
          newCart[i] = cartItem;
        }
      }

      console.log(newCart);

      updateDoc(doc(db, "users", args.user), {
        cart: newCart,
      })
        .then(() => resolve(true))
        .catch(() => resolve(false));
    }
  });
};

const markDelivered = async (args) => {
  let docSnap = await getDoc(doc(db, "users", args.user));

  return new Promise((resolve, reject) => {
    if (docSnap.exists()) {
      let newCart = [];

      for (let i = 0; i < docSnap.data().cart.length; i++) {
        let cartItem = {};
        if (
          docSnap.data().cart[i].paid == true &&
          docSnap.data().cart[i].packed == true &&
          docSnap.data().cart[i].delivered == false
        ) {
          cartItem.product = docSnap.data().cart[i].product;
          cartItem.added = docSnap.data().cart[i].added;
          cartItem.quantity = docSnap.data().cart[i].quantity;
          cartItem.paid = docSnap.data().cart[i].paid;
          cartItem.packed = docSnap.data().cart[i].packed;
          cartItem.delivered = true;
          newCart[i] = cartItem;
        } else {
          cartItem.product = docSnap.data().cart[i].product;
          cartItem.added = docSnap.data().cart[i].added;
          cartItem.quantity = docSnap.data().cart[i].quantity;
          cartItem.paid = docSnap.data().cart[i].paid;
          cartItem.packed = docSnap.data().cart[i].packed;
          cartItem.delivered = docSnap.data().cart[i].delivered;
          newCart[i] = cartItem;
        }
      }

      console.log(newCart);

      updateDoc(doc(db, "users", args.user), {
        cart: newCart,
      })
        .then(() => resolve(true))
        .catch(() => resolve(false));
    }
  });
};

const removeFromCart = async (args) => {
  let docSnap = await getDoc(doc(db, "users", args.user));

  return new Promise((resolve, reject) => {
    if (docSnap.exists()) {
      let new_cart = [
        ...docSnap
          .data()
          .cart.filter((cartItem) => cartItem.product.id !== args.id),
      ];

      console.log(new_cart);

      updateDoc(doc(db, "users", args.user), {
        cart: new_cart,
      })
        .then(() => resolve(true))
        .catch(() => resolve(false));
    } else {
      resolve(false);
    }
  });
};

const verifyOnOffer = (startDate, endDate) => {
  if (
    Date.now() > new Date(startDate).getTime() &&
    Date.now() < new Date(endDate).getTime()
  ) {
    return true;
  } else {
    return false;
  }
};

const DB_Services = {
  createProduct,
  addToCart,
  createOrder,
  incrementOrder,
  decrementOrder,
  decrementCartProduct,
  incrementCartProduct,
  setDeliverFirst,
  removeFromCart,
  markPacked,
  markDelivered,
  deleteOrder,
  updateProduct,
  uploadProductImage,
  deleteProduct,
  addOffer,
  createUser,
  updateUser,
  checkPhone,
  verifyOnOffer,
  productsRef,
};

export default DB_Services;
